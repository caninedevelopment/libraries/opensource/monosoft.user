﻿namespace Monosoft.User.Application.Common.Helpers
{
    using System;
    using System.Text;

    public static class Hash
    {
        public static string CalculateHash(string password, string localsalt)
        {

            var hashAlgorithm = new Org.BouncyCastle.Crypto.Digests.Sha3Digest(512);
            byte[] input = Encoding.UTF8.GetBytes(localsalt + password);
            hashAlgorithm.BlockUpdate(input, 0, input.Length);
            byte[] result = new byte[64]; // 512 / 8 = 64
            hashAlgorithm.DoFinal(result, 0);
            string hashString = BitConverter.ToString(result);
            hashString = hashString.Replace("-", "").ToLowerInvariant();
            return hashString;

            //// step 1, calculate MD5 hash from input
            //MD5 md5 = System.Security.Cryptography.MD5.Create();
            //byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(localsalt + input);
            //byte[] hash = md5.ComputeHash(inputBytes);

            //// step 2, convert byte array to hex string
            //StringBuilder sb = new StringBuilder();
            //for (int i = 0; i < hash.Length; i++)
            //{
            //    sb.Append(hash[i].ToString("X2"));
            //}

            //return sb.ToString();
        }
    }
}
