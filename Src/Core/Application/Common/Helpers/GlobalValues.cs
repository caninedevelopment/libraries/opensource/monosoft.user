﻿namespace Monosoft.User.Application.Common.Helpers
{
    /// <summary>
    /// Contains the global values for this program
    /// </summary>
    public static class GlobalValues
    {
        /// <summary>
        /// Route information for invalidate
        /// </summary>
        public static readonly string RouteTokenInvalidateUser = "token.invalidate.user";

        /// <summary>
        /// Route information for invalidate
        /// </summary>
        public static readonly string RouteTokenInvalidateToken = "token.invalidate.token";

        /// <summary>
        /// Time a token is valid
        /// </summary>
        public static readonly int TokenValidTimeInMinutes = 360;
    }
}