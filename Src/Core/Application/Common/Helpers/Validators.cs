﻿namespace Monosoft.User.Application.Common.Helpers
{
    using System.Text.RegularExpressions;

    public class Validators
    {
        /// <summary>
        /// Validates an email adress.
        /// </summary>
        /// <param name="email">the email to be validated</param>
        /// <returns>true if it is valid</returns>
        public static bool ValidEmail(string email)
        {
            if (string.IsNullOrEmpty(email) == false)
            {
                Regex regex = new Regex(@"^(([\w-]+\.)+[\w-]+|([\w-]+\+)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@"
                                  + @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]? [0-9]{1,2}|25[0-5]|2[0-4][0-9])\."
                                  + @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]? [0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                                  + @"([a-zA-Z0-9]+[\w-]+\.)+[a-zA-Z]{1}[a-zA-Z0-9-]{1,23})$");
                Match match = regex.Match(email);
                return match.Success;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Validates a phone number.
        /// </summary>
        /// <param name="phone">the phone to be validated</param>
        /// <returns>true if it is valid</returns>
        public static bool ValidPhone(string phone)
        {
            if (string.IsNullOrEmpty(phone) == false)
            {
                Regex regex = new Regex(@"^\s*\+?[0-9]\d?[- .]?(\([2-9]\d{2}\)|[2-9]\d{2})[- .]?\d{3}[- .]?\d{4}$");
                Match match = regex.Match(phone);
                return match.Success;
            }
            else
            {
                return false;
            }
        }
    }
}
