﻿namespace Monosoft.User.Application.Common.Services
{
    public interface IEventService
    {
        void RaiseEvent(string route, string json);
    }
}
