﻿namespace Monosoft.User.Application.Common.BaseDTO
{
    using Domain.Entities;
    using System;
    using System.Collections.Generic;

    public class BaseTokenDTO
    {
        public Guid TokenId { get; set; }
        public Guid UserId { get; set; }
        public DateTime ValidUntil { get; set; }
        public List<string> Claims { get; set; }

        public BaseTokenDTO(Token token)
        {
            this.TokenId = token.Id;
            this.UserId = token.FK_User;
            this.ValidUntil = token.ValidUntil;
            this.Claims = token.Claims;
        }
    }
}
