namespace Monosoft.User.Application.Resources.TrustedAuth.Commands.Register
{
    using Monosoft.Common.Command.Interfaces;
    using Monosoft.Common.Exceptions;
    using System.Collections.Generic;

    public class Request : IDtoRequest
    {
        public string ServerUrl { get; set; }
        public List<string> Trust { get; set; }

        public void Validate()
        {
            if (string.IsNullOrEmpty(ServerUrl))
            {
                throw new ValidationException("ServerUrl");
            }
        }
    }
}