namespace Monosoft.User.Application.Resources.TrustedAuth.Commands.Register
{
    using Domain.Interfaces;
    using Domain.Entities;
    using Monosoft.Common.Command.Interfaces;

    public class Command : IProcedure<Request>
    {
        private readonly ITrustedAuthRepository taRepo;

        public Command(
            ITrustedAuthRepository taRepo)
        {
            this.taRepo = taRepo;
        }

        public void Execute(Request input)
        {
            taRepo.Insert(new TrustedAuth(input.ServerUrl, input.Trust));
        }
    }
}