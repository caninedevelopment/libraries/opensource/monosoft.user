namespace Monosoft.User.Application.Resources.TrustedAuth.Commands.Verify
{
    using Monosoft.Common.Command.Interfaces;
    using Monosoft.Common.Exceptions;
    using System;

    public class Request : IDtoRequest
    {
        public string ExternalUrl { get; set; }
        public Guid TokenId { get; set; }

        public void Validate()
        {
            if (string.IsNullOrEmpty(ExternalUrl))
            {
                throw new ValidationException("ExternalUrl");
            }

            if (TokenId == Guid.Empty)
            {
                throw new ValidationException("TokenId");
            }
        }
    }
}