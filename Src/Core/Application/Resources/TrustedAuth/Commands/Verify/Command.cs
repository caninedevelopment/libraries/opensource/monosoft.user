namespace Monosoft.User.Application.Resources.TrustedAuth.Commands.Verify
{
    using Domain.Externals;
    using Domain.Interfaces;
    using Domain.Entities;
    using Monosoft.Common.Command.Interfaces;
    using Newtonsoft.Json;
    using System.Linq;
    using System.Net.Http;
    using System.Text;
    using Monosoft.Common.Exceptions;
    using Monosoft.User.Application.Common.BaseDTO;

    public class Command : IFunction<Request, Response>
    {
        private readonly ITrustedAuthRepository taRepo;
        private readonly ITokenRepository tokenRepo;

        public Command(
            ITrustedAuthRepository taRepo,
            ITokenRepository tokenRepo)
        {
            this.taRepo = taRepo;
            this.tokenRepo = tokenRepo;
        }

        public Response Execute(Request input)
        {
            var trustedAuth = taRepo.GetByUrl(input.ExternalUrl);

            if (trustedAuth != null)
            {
                var json = JsonConvert.SerializeObject(new VerifyInfo(input.TokenId));
                var data = new StringContent(json, Encoding.UTF8, "application/json");
                var url = trustedAuth.ServerUrl + "/api/token/v1/verify"; // TODO Version + nye URL?
                using var client = new HttpClient();
                var response = client.PostAsync(url, data).Result;
                string result = response.Content.ReadAsStringAsync().Result;

                // TODO make in infrastructur folder
                GatewayResponse remoteResult = JsonConvert.DeserializeObject<GatewayResponse>(result);

                if (remoteResult.Success == false)
                {
                    throw new System.Exception(remoteResult.Message[0].Text);
                }
                else
                {
                    TokenInfo remoteTokenInfo = JsonConvert.DeserializeObject<TokenInfo>(remoteResult.Data);
                    var whitelist = trustedAuth.Trust;
                    var acceptedClaims = remoteTokenInfo.Claims.Where(x => trustedAuth.Trust.Contains(x)).ToList();
                    var newToken = new Token(remoteTokenInfo.TokenId, remoteTokenInfo.UserId, remoteTokenInfo.ValidUntil, acceptedClaims);

                    var oldToken = tokenRepo.GetById(remoteTokenInfo.TokenId);
                    if (oldToken == null)
                    {
                        tokenRepo.Insert(newToken);
                    } else
                    {
                        tokenRepo.Update(newToken);
                    }

                    return new Response(newToken);
                }
            }
            else
            {
                throw new ElementDoesNotExistException("Server is not trusted", input.ExternalUrl);
            }
        }
    }
}