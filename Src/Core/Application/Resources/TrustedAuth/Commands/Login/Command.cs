namespace Monosoft.User.Application.Resources.TrustedAuth.Commands.Login
{
    using Domain.Entities;
    using Domain.Externals;
    using Domain.Interfaces;
    using Monosoft.Common.Command.Interfaces;
    using Monosoft.Common.Exceptions;
    using Newtonsoft.Json;
    using System.Linq;
    using System.Net.Http;
    using System.Text;

    public class Command : IFunction<Request, Response>
    {
        private readonly ITrustedAuthRepository tARepo;
        private readonly ITokenRepository tokenRepo;

        public Command(ITrustedAuthRepository TARepo,
            ITokenRepository tokenRepo)
        {
            tARepo = TARepo;
            this.tokenRepo = tokenRepo;
        }

        public Response Execute(Request input)
        {
            var trustedAuth = tARepo.GetByUrl(input.ExternalUrl);

            if (trustedAuth != null) {
                var json = JsonConvert.SerializeObject(new LoginInfo(input.Username, input.Password));
                var data = new StringContent(json, Encoding.UTF8, "application/json");
                var url = trustedAuth.ServerUrl + "/api/token/v1/login"; // TODO Version + nye URL?
                using var client = new HttpClient();
                var response = client.PostAsync(url, data).Result;
                string result = response.Content.ReadAsStringAsync().Result;

                // TODO make in infrastructur folder
                GatewayResponse remoteResult = JsonConvert.DeserializeObject<GatewayResponse>(result);

                if (remoteResult.Success == false)
                {
                    throw new System.Exception(remoteResult.Message[0].Text);
                } else
                {
                    TokenInfo remoteTokenInfo = JsonConvert.DeserializeObject<TokenInfo>(remoteResult.Data);
                    var whitelist = trustedAuth.Trust;
                    var acceptedClaims = remoteTokenInfo.Claims.Where(x => trustedAuth.Trust.Contains(x)).ToList();
                    var newToken = new Token(remoteTokenInfo.TokenId, remoteTokenInfo.UserId, remoteTokenInfo.ValidUntil, acceptedClaims);
                    tokenRepo.Insert(newToken);

                    return new Response(newToken);
                }
            } else
            {
                throw new ElementDoesNotExistException("Server is not trusted", input.ExternalUrl);
            }
        }
    }
}