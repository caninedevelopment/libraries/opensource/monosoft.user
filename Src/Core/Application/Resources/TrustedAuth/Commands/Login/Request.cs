namespace Monosoft.User.Application.Resources.TrustedAuth.Commands.Login
{
    using Monosoft.Common.Command.Interfaces;
    using Monosoft.Common.Exceptions;

    public class Request : IDtoRequest
    {
        public string ExternalUrl { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }

        public void Validate()
        {
            if (string.IsNullOrEmpty(ExternalUrl))
            {
                throw new ValidationException("ExternalUrl");
            }

            if (string.IsNullOrEmpty(Username))
            {
                throw new ValidationException("Username");
            }

            if (string.IsNullOrEmpty(Password))
            {
                throw new ValidationException("Password");
            }
        }
    }
}