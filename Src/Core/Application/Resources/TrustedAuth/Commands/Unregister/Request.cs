namespace Monosoft.User.Application.Resources.TrustedAuth.Commands.Unregister
{
    using Monosoft.Common.Command.Interfaces;
    using Monosoft.Common.Exceptions;

    public class Request : IDtoRequest
    {
        public string ServerUrl { get; set; }

        public void Validate()
        {
            if(string.IsNullOrEmpty(ServerUrl))
            {
                throw new ValidationException("ServerUrl");
            }
        }
    }
}