namespace Monosoft.User.Application.Resources.TrustedAuth.Commands.Unregister
{
    using Domain.Interfaces;
    using Monosoft.Common.Command.Interfaces;

    public class Command : IProcedure<Request>
    {
        private readonly ITrustedAuthRepository taRepo;

        public Command(
            ITrustedAuthRepository taRepo)
        {
            this.taRepo = taRepo;
        }

        public void Execute(Request input)
        {
            taRepo.Delete(input.ServerUrl);
        }
    }
}