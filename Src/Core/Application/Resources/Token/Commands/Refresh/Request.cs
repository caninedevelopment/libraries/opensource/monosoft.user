namespace Monosoft.User.Application.Resources.Token.Commands.Refresh
{
    using Monosoft.Common.Command.Interfaces;
    using Monosoft.Common.Interfaces;

    public class Request : IDtoRequest, IHeader
    {
        public string Token { get; set; }
        public string User { get; set; }

        public void Validate()
        {
        }
    }
}