namespace Monosoft.User.Application.Resources.Token.Commands.Refresh
{
    using Monosoft.User.Application.Common.Helpers;
    using Domain.Interfaces;
    using Monosoft.Common.Command.Interfaces;
    using Monosoft.Common.Exceptions;
    using System;
    using Monosoft.User.Application.Common.Services;
    using Newtonsoft.Json;

    public class Command : IFunction<Request, Response>
    {
        private readonly ITokenRepository tokenRepo;
        private readonly IEventService eventService;

        public Command(
            ITokenRepository tokenRepo,
            IEventService eventService)
        {
            this.tokenRepo = tokenRepo;
            this.eventService = eventService;
        }

        public Response Execute(Request input)
        {
            var token = tokenRepo.GetById(new Guid(input.Token));
            if (token != null)
            {
                token.ValidUntil = DateTime.Now.AddHours(GlobalValues.TokenValidTimeInMinutes);
                eventService.RaiseEvent(GlobalValues.RouteTokenInvalidateToken, JsonConvert.SerializeObject(new Response(token)));
                tokenRepo.Update(token);
                return new Response(token);
            } else
            {
                throw new ElementDoesNotExistException("Uknown token", input.Token);
            }
        }
    }
}