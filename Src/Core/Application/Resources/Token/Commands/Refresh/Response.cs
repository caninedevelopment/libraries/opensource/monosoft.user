namespace Monosoft.User.Application.Resources.Token.Commands.Refresh
{
    using Monosoft.Common.Command.Interfaces;
    using Domain.Entities;
    using Monosoft.User.Application.Common.BaseDTO;

    public class Response : BaseTokenDTO, IDtoResponse
    {
        public Response(Token token)
            :base(token)
        {

        }
    }
}