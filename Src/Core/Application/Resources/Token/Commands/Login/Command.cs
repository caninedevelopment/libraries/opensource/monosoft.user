namespace Monosoft.User.Application.Resources.Token.Commands.Login
{
    using Domain.Interfaces;
    using Domain.Entities;
    using Monosoft.Common.Command.Interfaces;
    using Monosoft.Common.Exceptions;
    using System;
    using System.Linq;
    using Monosoft.User.Application.Common.Helpers;
    using System.Collections.Generic;
    using Monosoft.User.Application.Common.Services;

    public class Command : IFunction<Request, Response>
    {
        private readonly IUserRepository userRepo;
        private readonly IUserLoginLogRepository logRepo;
        private readonly IUserGroupRepository ugRepo;
        private readonly ITokenRepository tokenRepo;
        private readonly IEventService eventService;
        private readonly IUserInUserGroupRepository uinugRepo;

        public Command(
            IUserRepository userRepo,
            IUserLoginLogRepository logRepo,
            IUserInUserGroupRepository uinugRepo,
            IUserGroupRepository ugRepo,
            ITokenRepository tokenRepo,
            IEventService eventService)
        {
            this.userRepo = userRepo;
            this.logRepo = logRepo;
            this.ugRepo = ugRepo;
            this.tokenRepo = tokenRepo;
            this.eventService = eventService;
            this.uinugRepo = uinugRepo;
        }

        public Response Execute(Request input)
        {
            var user = userRepo.GetByUsername(input.Username);

            if (user == null)
            {
                logRepo.Insert(new UserLoginLog(DateTime.Now, input.InitiatedByIp, false, null));
                throw new ElementDoesNotExistException("Invalid credentials", input.Username);
            }

            string pwdHash = Hash.CalculateHash(input.Password, user.Id.ToString());
            if (user.PWDHash != pwdHash)
            {
                logRepo.Insert(new UserLoginLog(DateTime.Now, input.InitiatedByIp, false, user));
                throw new ElementDoesNotExistException("Invalid credentials", input.Username);
            }
            else
            {
                logRepo.Insert(new UserLoginLog(DateTime.Now, input.InitiatedByIp, true, user));

                var ugids = uinugRepo.GetUserGroupIds(user.Id);
                var claims = ugRepo.GetByIds(ugids).SelectMany(x => x.Claims).Distinct();

                var token = new Token(Guid.NewGuid(), user.Id, DateTime.Now.AddHours(GlobalValues.TokenValidTimeInMinutes), claims.ToList());

                var oldTokens = tokenRepo.GetByUserIdFromDateTime(user.Id, DateTime.Now);

                tokenRepo.Insert(token);

                SendEvents(oldTokens);

                return new Response(token);
            }
        }

        private void SendEvents(List<Token> tokens)
        {
            foreach (var oToken in tokens)
            {
                oToken.ValidUntil = DateTime.Now.AddMinutes(1);
                tokenRepo.Update(oToken);
                eventService.RaiseEvent(GlobalValues.RouteTokenInvalidateToken, Newtonsoft.Json.JsonConvert.SerializeObject(new Response(oToken)));
            }
        }
    }
}
