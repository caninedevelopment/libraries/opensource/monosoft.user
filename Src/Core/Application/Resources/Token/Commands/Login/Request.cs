namespace Monosoft.User.Application.Resources.Token.Commands.Login
{
    using Monosoft.Common.Command.Interfaces;
    using Monosoft.Common.Exceptions;
    using Monosoft.Common.Interfaces;

    public class Request : IDtoRequest, IClient
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string InitiatedByIp { get; set; }

        public void Validate()
        {
            if (string.IsNullOrEmpty(Username))
            {
                throw new ValidationException("Username");
            }

            if (string.IsNullOrEmpty(Password))
            {
                throw new ValidationException("Password");
            }
        }
    }
}