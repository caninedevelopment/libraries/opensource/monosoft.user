namespace Monosoft.User.Application.Resources.Token.Commands.Verify
{
    using Domain.Interfaces;
    using Monosoft.Common.Command.Interfaces;
    using Monosoft.Common.Exceptions;
    using System;

    public class Command : IFunction<Request, Response>
    {
        private readonly ITokenRepository tokenRepo;

        public Command(ITokenRepository tokenRepo)
        {
            this.tokenRepo = tokenRepo;
        }

        public Response Execute(Request input)
        {
            var token = tokenRepo.GetById(new Guid(input.Token));
            if (token != null)
            {
                return new Response(token);
            } else
            {
                throw new ElementDoesNotExistException("Uknown token", input.Token);
            }
        }
    }
}