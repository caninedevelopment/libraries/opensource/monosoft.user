namespace Monosoft.User.Application.Resources.UserGroup.Commands.Insert
{
    using Monosoft.User.Application.Resources.UserGroup.Common;
    using Monosoft.Common.Command.Interfaces;
    using Monosoft.Common.Exceptions;
    using System;

    public class Request : UserGroupDTO, IDtoRequest
    {

        public void Validate()
        {
            if (string.IsNullOrEmpty(Name))
            {
                throw new ValidationException("Name");
            }

            if (Id == Guid.Empty)
            {
                throw new ValidationException("Id");
            }
        }
    }
}