namespace Monosoft.User.Application.Resources.UserGroup.Commands.Insert
{
    using Domain.Interfaces;
    using Domain.Entities;
    using Monosoft.Common.Command.Interfaces;
    using Monosoft.Common.Exceptions;

    public class Command : IProcedure<Request>
    {
        private readonly IUserGroupRepository ugRepo;

        public Command(
            IUserGroupRepository ugRepo)
        {
            this.ugRepo = ugRepo;
        }

        public void Execute(Request input)
        {
            var ug = ugRepo.GetById(input.Id);
            if (ug == null)
            {
                ug = new UserGroup()
                {
                    Id = input.Id,
                    Name = input.Name,
                    Claims = input.Claims
                };

                ugRepo.Insert(ug);
            } else
            {
                throw new ElementAlreadyExistsException("The usergroup already exists", input.Id.ToString());
            }
        }
    }
}