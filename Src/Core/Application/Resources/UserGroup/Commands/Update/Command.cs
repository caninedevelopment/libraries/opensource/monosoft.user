namespace Monosoft.User.Application.Resources.UserGroup.Commands.Update
{
    using Monosoft.User.Application.Common.Helpers;
    using Domain.Interfaces;
    using Monosoft.Common.Command.Interfaces;
    using Monosoft.Common.Exceptions;
    using System;
    using System.Linq;
    using Monosoft.User.Application.Common.Services;
    using Newtonsoft.Json;
    using Monosoft.User.Domain.Externals;

    public class Command : IProcedure<Request>
    {
        private readonly IUserGroupRepository ugRepo;
        private readonly IUserInUserGroupRepository uinugRepo;
        private readonly IEventService eventService;

        public Command(
            IUserGroupRepository ugRepo,
            IUserInUserGroupRepository uinugRepo,
            IEventService eventService)
        {
            this.ugRepo = ugRepo;
            this.uinugRepo = uinugRepo;
            this.eventService = eventService;
        }

        public void Execute(Request input)
        {
            var ug = ugRepo.GetById(input.Id);
            if (ug != null)
            {
                ug.Name = input.Name;
                ug.Claims = input.Claims;

                ugRepo.Update(ug);

                var uinug = uinugRepo.GetByUserGroupId(input.Id).Select(x => x.FK_User);

                var affectedusers = new InvalidateUserData()
                {
                    ValidUntil = DateTime.Now,
                    UserIds = uinug.ToArray(),
                };

                eventService.RaiseEvent(GlobalValues.RouteTokenInvalidateUser, JsonConvert.SerializeObject(affectedusers));
            } else
            {
                throw new ElementDoesNotExistException("No usergroup with that id", input.Id.ToString());
            }
        }
    }
}