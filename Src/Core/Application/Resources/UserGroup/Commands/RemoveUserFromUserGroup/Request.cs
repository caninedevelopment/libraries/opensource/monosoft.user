namespace Monosoft.User.Application.Resources.UserGroup.Commands.RemoveUserFromUserGroup
{
    using Monosoft.Common.Command.Interfaces;
    using Monosoft.Common.Exceptions;
    using System;

    public class Request : IDtoRequest
    {
        public Guid UsergroupId { get; set; }

        public Guid UserId { get; set; }

        public void Validate()
        {
            if (UserId == Guid.Empty)
            {
                throw new ValidationException("UserId");
            }

            if (UsergroupId == Guid.Empty)
            {
                throw new ValidationException("UsergroupId");
            }
        }
    }
}