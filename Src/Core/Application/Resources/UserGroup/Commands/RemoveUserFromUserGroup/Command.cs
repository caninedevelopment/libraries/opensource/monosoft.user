namespace Monosoft.User.Application.Resources.UserGroup.Commands.RemoveUserFromUserGroup
{
    using Monosoft.User.Application.Common.Helpers;
    using Domain.Interfaces;
    using Monosoft.Common.Command.Interfaces;
    using Monosoft.Common.Exceptions;
    using System;
    using System.Linq;
    using Monosoft.User.Application.Common.Services;
    using Newtonsoft.Json;
    using Monosoft.User.Domain.Externals;

    public class Command : IProcedure<Request>
    {
        private readonly IUserInUserGroupRepository uinugRepo;
        private readonly IEventService eventService;

        public Command(
            IUserInUserGroupRepository uinugRepo,
            IEventService eventService)
        {
            this.uinugRepo = uinugRepo;
            this.eventService = eventService;
        }

        public void Execute(Request input)
        {
            var uinug = uinugRepo.GetByUserAndUserGroup(input.UserId, input.UsergroupId);

            if (uinug != null)
            {
                var userIds = uinugRepo.GetByUserGroupId(input.UsergroupId).Select(x => x.FK_User);

                var affectedusers = new InvalidateUserData()
                {
                    ValidUntil = DateTime.Now,
                    UserIds = userIds.ToArray(),
                };

                uinugRepo.Delete(uinug);

                eventService.RaiseEvent(GlobalValues.RouteTokenInvalidateUser, JsonConvert.SerializeObject(affectedusers));
            } else
            {
                throw new ElementDoesNotExistException("User is not in usergroup", input.UsergroupId.ToString());
            }
        }
    }
}