namespace Monosoft.User.Application.Resources.UserGroup.Commands.GetAll
{
    using Monosoft.User.Application.Resources.UserGroup.Common;
    using Domain.Interfaces;
    using Monosoft.Common.Command.Interfaces;
    using System.Linq;

    public class Command : IFunction<Response>
    {
        private readonly IUserGroupRepository ugRepo;

        public Command(
            IUserGroupRepository ugRepo)
        {
            this.ugRepo = ugRepo;
        }

        public Response Execute()
        {
            var ugs = ugRepo.GetAll();

            // TODO get all users ?
            var ugdtos = ugs.Select(ug => new UserGroupDTO(ug));

            return new Response(ugdtos);
        }
    }
}