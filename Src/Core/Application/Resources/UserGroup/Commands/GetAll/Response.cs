namespace Monosoft.User.Application.Resources.UserGroup.Commands.GetAll
{
    using Monosoft.User.Application.Resources.UserGroup.Common;
    using Monosoft.Common.Command.Interfaces;
    using System.Collections.Generic;
    using System.Linq;

    public class Response : IDtoResponse
    {
        public List<UserGroupDTO> UserGroups { get; set; }

        public Response(IEnumerable<UserGroupDTO> usergroups)
        {
            this.UserGroups = usergroups.ToList();
        }
    }
}