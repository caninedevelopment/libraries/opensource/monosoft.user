namespace Monosoft.User.Application.Resources.UserGroup.Commands.AddUserToUserGroup
{
    using Monosoft.User.Application.Common.Helpers;
    using Domain.Entities;
    using Domain.Interfaces;
    using Monosoft.Common.Command.Interfaces;
    using Monosoft.Common.Exceptions;
    using System;
    using Monosoft.User.Application.Common.Services;
    using Newtonsoft.Json;
    using Monosoft.User.Domain.Externals;

    public class Command : IProcedure<Request>
    {
        private readonly IUserInUserGroupRepository uinugRepo;
        private readonly IEventService eventService;

        public Command(
            IUserInUserGroupRepository uinugRepo,
            IEventService eventService)
        {
            this.uinugRepo = uinugRepo;
            this.eventService = eventService;
        }

        public void Execute(Request input)
        {
            var ug = uinugRepo.GetByUserAndUserGroup(input.userId, input.usergroupId);

            if (ug == null)
            {
                var uinug = new UserInUserGroup(input.userId, input.usergroupId);

                uinugRepo.Insert(uinug);

                var affectedusers = new InvalidateUserData()
                {
                    ValidUntil = DateTime.Now,
                    UserIds = new Guid[] { input.userId }
                };

                eventService.RaiseEvent(GlobalValues.RouteTokenInvalidateUser, JsonConvert.SerializeObject(affectedusers));
            } else
            {
                throw new ElementAlreadyExistsException("User is already in the group", input.userId.ToString());
            }
        }
    }
}