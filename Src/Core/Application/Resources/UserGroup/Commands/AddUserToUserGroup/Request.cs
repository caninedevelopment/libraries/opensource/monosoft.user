namespace Monosoft.User.Application.Resources.UserGroup.Commands.AddUserToUserGroup
{
    using Monosoft.Common.Command.Interfaces;
    using Monosoft.Common.Exceptions;
    using System;

    public class Request : IDtoRequest
    {
        public Guid usergroupId { get; set; }

        public Guid userId { get; set; }

        public void Validate()
        {
            if (userId == Guid.Empty)
            {
                throw new ValidationException("UserId");
            }

            if (usergroupId == Guid.Empty)
            {
                throw new ValidationException("usergroupId");
            }
        }
    }
}