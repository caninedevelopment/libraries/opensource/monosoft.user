namespace Monosoft.User.Application.Resources.UserGroup.Commands.Delete
{
    using Monosoft.Common.Command.Interfaces;
    using Monosoft.Common.Exceptions;
    using System;

    public class Request : IDtoRequest
    {
        public Guid UserGroupId { get; set; }

        public void Validate()
        {
            if (UserGroupId == Guid.Empty)
            {
                throw new ValidationException("UserGroupId");
            }
        }
    }
}