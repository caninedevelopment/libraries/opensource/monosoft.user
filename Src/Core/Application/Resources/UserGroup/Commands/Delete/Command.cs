namespace Monosoft.User.Application.Resources.UserGroup.Commands.Delete
{
    using Monosoft.User.Application.Common.Helpers;
    using Domain.Interfaces;
    using Monosoft.Common.Command.Interfaces;
    using Monosoft.Common.Exceptions;
    using System;
    using System.Linq;
    using Monosoft.User.Application.Common.Services;
    using Newtonsoft.Json;
    using Monosoft.User.Domain.Externals;

    public class Command : IProcedure<Request>
    {
        private readonly IUserGroupRepository ugRepo;
        private readonly IUserInUserGroupRepository uinugRepo;
        private readonly IEventService eventService;

        public Command(
            IUserGroupRepository ugRepo,
            IUserInUserGroupRepository uinugRepo,
            IEventService eventService)
        {
            this.ugRepo = ugRepo;
            this.uinugRepo = uinugRepo;
            this.eventService = eventService;
        }

        public void Execute(Request input)
        {
            var ug = ugRepo.GetById(input.UserGroupId);
            if (ug != null)
            {
                var users = uinugRepo.GetByUserGroupId(input.UserGroupId).Select(x => x.FK_User);
                uinugRepo.DeleteByUserGroup(input.UserGroupId);
                ugRepo.Delete(ug);

                var affectedusers = new InvalidateUserData()
                {
                    ValidUntil = DateTime.Now,
                    UserIds = users.ToArray()
                };

                if (affectedusers != null)
                {
                    eventService.RaiseEvent(GlobalValues.RouteTokenInvalidateUser, JsonConvert.SerializeObject(affectedusers));
                }
            } else
            {
                throw new ElementDoesNotExistException("No usergroup with that id", input.UserGroupId.ToString());
            }
        }
    }
}