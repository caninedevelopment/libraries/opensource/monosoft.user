﻿namespace Monosoft.User.Application.Resources.UserGroup.Common
{
    using Domain.Entities;
    using System;
    using System.Collections.Generic;

    public class UserGroupDTO
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public List<string> Claims { get; set; }

        public UserGroupDTO() { }

        public UserGroupDTO(UserGroup ug)
        {
            this.Id = ug.Id;
            this.Name = ug.Name;
            this.Claims = ug.Claims;
        }
    }
}
