namespace Monosoft.User.Application.Resources.User.Commands.Update
{
    using Monosoft.User.Application.Common.Helpers;
    using Monosoft.User.Application.Resources.User.Common;
    using Monosoft.Common.Command.Interfaces;
    using Monosoft.Common.Exceptions;
    using System;
    using System.Collections.Generic;

    public class Request : UserDTO, IDtoRequest
    {
        public List<string> Claims { get; set; }

        public void Validate()
        {
            if (string.IsNullOrEmpty(Username))
            {
                throw new ValidationException("Username");
            }

            if (Validators.ValidEmail(Email) == false)
            {
                throw new ValidationException("Email");
            }

            if (Validators.ValidPhone(Mobile) == false)
            {
                throw new ValidationException("Mobile");
            }

            if (UserId == Guid.Empty)
            {
                throw new ValidationException("UserId");
            }
        }
    }
}