namespace Monosoft.User.Application.Resources.User.Commands.Update
{
    using Domain.Interfaces;
    using Monosoft.Common.Command.Interfaces;
    using Monosoft.Common.Exceptions;
    using Newtonsoft.Json;

    public class Command : IProcedure<Request>
    {
        private readonly IUserRepository uRepo;

        public Command(
            IUserRepository uRepo)
        {
            this.uRepo = uRepo;
        }

        public void Execute(Request input)
        {
            var user = uRepo.GetByUsername(input.Username);
            if (user == null || user.Id == input.UserId)
            {
                user.Email = input.Email;
                user.Mobile = input.Mobile;
                user.Username = input.Username;
                user.Claims = input.Claims;
                user.MetadataString = JsonConvert.SerializeObject(input.Metadata);

                uRepo.Update(user);
            } else
            {
                throw new ElementAlreadyExistsException("Username already in use", input.Username);
            }
        }
    }
}