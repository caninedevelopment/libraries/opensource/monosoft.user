namespace Monosoft.User.Application.Resources.User.Commands.GetMyData
{
    using Monosoft.User.Application.Resources.User.Common;
    using Domain.Entities;
    using Monosoft.Common.Command.Interfaces;
    using System.Collections.Generic;

    public class Response : UserDTO, IDtoResponse
    {
        public List<string> Claims { get; set; }

        public Response(User user, List<string> claims)
            :base(user)
        {
            this.Claims = claims;
        }
    }
}