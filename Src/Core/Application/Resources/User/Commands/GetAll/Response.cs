namespace Monosoft.User.Application.Resources.User.Commands.GetAll
{
    using Monosoft.User.Application.Resources.User.Common;
    using Monosoft.Common.Command.Interfaces;
    using System.Collections.Generic;

    public class Response : IDtoResponse
    {
        public List<UserDTO> Users { get; set; }

        public Response(List<UserDTO> userdtos)
        {
            this.Users = userdtos;
        }
    }
}