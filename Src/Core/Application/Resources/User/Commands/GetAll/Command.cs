namespace Monosoft.User.Application.Resources.User.Commands.GetAll
{
    using Monosoft.User.Application.Resources.User.Common;
    using Domain.Interfaces;
    using Monosoft.Common.Command.Interfaces;
    using System.Linq;

    public class Command : IFunction<Response>
    {
        private readonly IUserRepository uRepo;
        private readonly IUserInUserGroupRepository uinugRepo;
        private readonly IUserGroupRepository ugRepo;

        public Command(
            IUserRepository uRepo,
            IUserInUserGroupRepository uinugRepo,
            IUserGroupRepository ugRepo)
        {
            this.uRepo = uRepo;
            this.uinugRepo = uinugRepo;
            this.ugRepo = ugRepo;
        }

        public Response Execute()
        {
            var users = uRepo.GetAll();

            var userdtos = users.Select(x => new UserDTO(x)).ToList();

            // TODO add claims again ?
            //List<UserDTO> userdtos = new List<UserDTO>();
            //foreach(var user in users)
            //{
            //    userdtos.Add(new UserDTO(user));
            //}

            return new Response(userdtos);
        }
    }
}