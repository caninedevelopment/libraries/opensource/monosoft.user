namespace Monosoft.User.Application.Resources.User.Commands.ResetPassword
{
    using Domain.Interfaces;
    using Monosoft.Common.Command.Interfaces;
    using Monosoft.Common.Exceptions;
    using System;

    public class Command : IFunction<Request, Response>
    {
        private readonly IUserRepository uRepo;

        public Command(
            IUserRepository uRepo)
        {
            this.uRepo = uRepo;
        }

        public Response Execute(Request input)
        {
            var user = uRepo.GetByEmail(input.Email);
            if (user != null)
            {
                user.ResetToken = Guid.NewGuid().ToString() + Guid.NewGuid().ToString();
                uRepo.Update(user);

                return new Response(user.ResetToken);
            } else
            {
                throw new ElementDoesNotExistException("No user with that email", input.Email);
            }
        }
    }
}