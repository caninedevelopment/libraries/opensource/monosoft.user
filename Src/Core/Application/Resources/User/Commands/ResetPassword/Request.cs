namespace Monosoft.User.Application.Resources.User.Commands.ResetPassword
{
    using Monosoft.User.Application.Common.Helpers;
    using Monosoft.Common.Command.Interfaces;
    using Monosoft.Common.Exceptions;

    public class Request : IDtoRequest
    {
        public string Email { get; set; }

        public void Validate()
        {
            if (Validators.ValidEmail(Email) == false)
            {
                throw new ValidationException("Email");
            }
        }
    }
}