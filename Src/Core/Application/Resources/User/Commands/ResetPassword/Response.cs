namespace Monosoft.User.Application.Resources.User.Commands.ResetPassword
{
    using Monosoft.Common.Command.Interfaces;

    public class Response : IDtoResponse
    {
        public string ResetToken { get; set; }
        public Response(string resetToken)
        {
            this.ResetToken = ResetToken;
        }
    }
}