namespace Monosoft.User.Application.Resources.User.Commands.Delete
{
    using Monosoft.Common.Command.Interfaces;
    using Monosoft.Common.Exceptions;
    using System;

    public class Request : IDtoRequest
    {
        public Guid UserId { get; set; }
        public void Validate()
        {
            if (UserId == Guid.Empty)
            {
                throw new ValidationException("UserId");
            }
        }
    }
}