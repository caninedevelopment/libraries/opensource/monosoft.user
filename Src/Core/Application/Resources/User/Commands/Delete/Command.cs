namespace Monosoft.User.Application.Resources.User.Commands.Delete
{
    using Domain.Interfaces;
    using Monosoft.Common.Command.Interfaces;
    using Monosoft.Common.Exceptions;

    public class Command : IProcedure<Request>
    {
        private readonly IUserRepository uRepo;

        public Command(
            IUserRepository uRepo)
        {
            this.uRepo = uRepo;
        }

        public void Execute(Request input)
        {
            var user = uRepo.GetById(input.UserId);
            if (user != null)
            {
                uRepo.Delete(user);
            } else
            {
                throw new ElementDoesNotExistException("No user with that Id", input.UserId.ToString());
            }
        }
    }
}