namespace Monosoft.User.Application.Resources.User.Commands.AnonymCreate
{
    using Monosoft.User.Application.Common.Helpers;
    using Domain.Entities;
    using Domain.Interfaces;
    using Monosoft.Common.Command.Interfaces;
    using Monosoft.Common.Exceptions;
    using System;

    public class Command : IProcedure<Request>
    {
        private readonly ISettingsRepository setRepo;
        private readonly IUserRepository uRepo;
        private readonly IUserInUserGroupRepository uinugRepo;

        public Command(
            ISettingsRepository setRepo,
            IUserRepository uRepo,
            IUserInUserGroupRepository uinugRepo)
        {
            this.setRepo = setRepo;
            this.uRepo = uRepo;
            this.uinugRepo = uinugRepo;
        }

        public void Execute(Request input)
        {
            var settings = setRepo.GetSettings();
            if (settings == null || string.IsNullOrEmpty(settings.AnonymUserGroup))
            {
                throw new ElementDoesNotExistException("Please save settings before using anonym create", "Settings");
            }

            var oUser = uRepo.GetByUsername(input.Username);
            if (oUser == null)
            {
                oUser = uRepo.GetById(input.UserId);
                if (oUser == null)
                {
                    var user = new User()
                    {
                        Id = input.UserId,
                        Username = input.Username,
                        Email = input.Email,
                        Mobile = input.Mobile,
                        MetadataString = null,
                        PWDHash = Hash.CalculateHash(input.Password, input.UserId.ToString())
                    };

                    uRepo.Insert(user);

                    var userInGrp = new UserInUserGroup();
                    userInGrp.FK_User = input.UserId;
                    userInGrp.FK_UserGroup = new Guid(settings.AnonymUserGroup);

                    uinugRepo.Insert(userInGrp);
                } else
                {
                    throw new ElementAlreadyExistsException("Userid already in use", input.UserId.ToString());
                }
            } else
            {
                throw new ElementAlreadyExistsException("Username already taken", input.Username);
            }
        }
    }
}