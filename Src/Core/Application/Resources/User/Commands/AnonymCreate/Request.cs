namespace Monosoft.User.Application.Resources.User.Commands.AnonymCreate
{
    using Monosoft.Common.Command.Interfaces;
    using Monosoft.User.Application.Resources.User.Common;
    using Monosoft.Common.Exceptions;
    using Monosoft.User.Application.Common.Helpers;
    using System;

    public class Request : BaseUserDTO, IDtoRequest
    {
        public string Password { get; set; }

        public void Validate()
        {
            if (UserId == Guid.Empty)
            {
                throw new ValidationException("UserId");
            }

            if (Validators.ValidEmail(Email) == false)
            {
                // TODO is this correct? At least it shouldnt throw exception
                Email = null;
            }

            if (string.IsNullOrEmpty(Username))
            {
                throw new ValidationException("Username");
            }

            if (string.IsNullOrEmpty(Password))
            {
                throw new ValidationException("Password");
            }

            if (Validators.ValidPhone(Mobile) == false)
            {
                // TODO is this correct? At least it shouldnt throw exception
                Mobile = null;
            }
        }
    }
}