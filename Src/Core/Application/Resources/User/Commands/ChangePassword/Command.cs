namespace Monosoft.User.Application.Resources.User.Commands.ChangePassword
{
    using Monosoft.User.Application.Common.Helpers;
    using Domain.Interfaces;
    using Monosoft.Common.Command.Interfaces;
    using Monosoft.Common.Exceptions;
    using System;

    public class Command : IProcedure<Request>
    {
        private readonly IUserRepository uRepo;

        public Command(
            IUserRepository uRepo)
        {
            this.uRepo = uRepo;
        }

        public void Execute(Request input)
        {
            var hashPwd = Hash.CalculateHash(input.OldPassword, input.User);
            var user = uRepo.GetById(new Guid(input.User));
            if (user != null && user.PWDHash == hashPwd)
            {
                user.PWDHash = Hash.CalculateHash(input.NewPassword, input.User);
                uRepo.Update(user);
            } else
            {
                throw new ElementDoesNotExistException("Invalid credentials", input.User);
            }
        }
    }
}