namespace Monosoft.User.Application.Resources.User.Commands.ChangePassword
{
    using Monosoft.Common.Command.Interfaces;
    using Monosoft.Common.Exceptions;
    using Monosoft.Common.Interfaces;

    public class Request : IDtoRequest, IHeader
    {
        public string NewPassword { get; set; }
        public string OldPassword { get; set; }
        public string Token { get; set; }
        public string User { get; set; }

        public void Validate()
        {
            if (string.IsNullOrEmpty(NewPassword))
            {
                throw new ValidationException("NewPassword");
            }

            if (string.IsNullOrEmpty(OldPassword))
            {
                throw new ValidationException("OldPassword");
            }
        }
    }
}