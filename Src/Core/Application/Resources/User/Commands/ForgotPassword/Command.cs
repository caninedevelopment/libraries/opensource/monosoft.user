namespace Monosoft.User.Application.Resources.User.Commands.ForgotPassword
{
    using Monosoft.User.Application.Common.Helpers;
    using Domain.Interfaces;
    using Monosoft.Common.Command.Interfaces;
    using Monosoft.Common.Exceptions;
    using System;

    public class Command : IProcedure<Request>
    {
        private readonly IUserRepository uRepo;
        private readonly ISettingsRepository setRepo;

        public Command(
            IUserRepository uRepo,
            ISettingsRepository setRepo)
        {
            this.uRepo = uRepo;
            this.setRepo = setRepo;
        }

        public void Execute(Request input)
        {
            var settings = setRepo.GetSettings();
            if (settings == null)
            {
                throw new ElementDoesNotExistException("Please save settings before using anonym create", "Settings");
            }

            var user = uRepo.GetByEmail(input.Email);
            if (user != null)
            {
                user.ResetToken = Guid.NewGuid().ToString() + Guid.NewGuid().ToString();
                uRepo.Update(user);

                System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient(settings.SMTPServerName, settings.SMTPPort);
                if (string.IsNullOrEmpty(settings.SMTPUserName) == false)
                {
                    smtp.Credentials = new System.Net.NetworkCredential(settings.SMTPUserName, settings.SMTPPassword);
                }
                smtp.EnableSsl = settings.SMTPPort != 80;

                var message = new System.Net.Mail.MailMessage();
                message.From = new System.Net.Mail.MailAddress(settings.ForgotPasswordSenderEmail, settings.ForgotPasswordNameOfSender);
                message.Body = settings.ForgotPasswordEmailBody.Replace("{0}", user.ResetToken);
                message.IsBodyHtml = true;
                message.Subject = settings.ForgotPasswordEmailSubject;
                message.To.Add(input.Email);
                smtp.Send(message);
            }
            else
            {
                throw new ElementDoesNotExistException("No user with that email", input.Email);
            }
        }
    }
}