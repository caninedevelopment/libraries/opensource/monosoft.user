﻿namespace Monosoft.User.Application.Resources.User.Commands.UpdateMyData
{
    using Monosoft.User.Application.Common.Helpers;
    using Monosoft.User.Application.Resources.User.Common;
    using Monosoft.Common.Command.Interfaces;
    using Monosoft.Common.Exceptions;
    using System.Collections.Generic;
    using Monosoft.Common.Interfaces;

    public class Request : UserDTO, IDtoRequest, IHeader
    {
        public List<string> Claims { get; set; }
        public string Token { get; set; }
        public string User { get; set; }

        public void Validate()
        {
            if (string.IsNullOrEmpty(Username))
            {
                throw new ValidationException("Username");
            }

            if (Validators.ValidEmail(Email) == false)
            {
                throw new ValidationException("Email");
            }

            if (Validators.ValidPhone(Mobile) == false)
            {
                throw new ValidationException("Mobile");
            }
        }
    }
}
