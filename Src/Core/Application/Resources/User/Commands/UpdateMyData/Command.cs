namespace Monosoft.User.Application.Resources.User.Commands.UpdateMyData
{
    using Domain.Interfaces;
    using Monosoft.Common.Command.Interfaces;
    using Monosoft.Common.Exceptions;
    using Newtonsoft.Json;
    using System;
    using System.Linq;

    public class Command : IProcedure<Request>
    {
        private readonly IUserRepository uRepo;

        public Command(
            IUserRepository uRepo)
        {
            this.uRepo = uRepo;
        }

        public void Execute(Request input)
        {
            var user = uRepo.GetById(new Guid(input.User));
            if (user != null)
            {
                user.Email = input.Email;
                user.Mobile = input.Mobile;
                user.Username = input.Username;
                user.Claims = input.Claims;
                user.MetadataString = JsonConvert.SerializeObject(input.Metadata);

                uRepo.Update(user);
            } else
            {
                throw new ElementDoesNotExistException("No user with that userid", input.User);
            }
        }
    }
}