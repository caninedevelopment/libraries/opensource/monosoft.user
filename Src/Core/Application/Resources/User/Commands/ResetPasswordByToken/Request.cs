namespace Monosoft.User.Application.Resources.User.Commands.ResetPasswordByToken
{
    using Monosoft.Common.Command.Interfaces;
    using Monosoft.Common.Exceptions;

    public class Request : IDtoRequest
    {
        public string Token { get; set; }
        public string NewPassword { get; set; }

        public void Validate()
        {
            if (string.IsNullOrEmpty(Token))
            {
                throw new ValidationException("Token");
            }

            if (string.IsNullOrEmpty(NewPassword))
            {
                throw new ValidationException("NewPassword");
            }
        }
    }
}