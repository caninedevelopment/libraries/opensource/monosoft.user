namespace Monosoft.User.Application.Resources.User.Commands.ResetPasswordByToken
{
    using Monosoft.User.Application.Common.Helpers;
    using Domain.Interfaces;
    using Monosoft.Common.Command.Interfaces;
    using Monosoft.Common.Exceptions;

    public class Command : IProcedure<Request>
    {
        private readonly IUserRepository uRepo;

        public Command(
            IUserRepository uRepo)
        {
            this.uRepo = uRepo;
        }

        public void Execute(Request input)
        {
            var user = uRepo.GetByResetToken(input.Token);
            if (user != null)
            {
                user.PWDHash = Hash.CalculateHash(input.NewPassword, user.Id.ToString());
                uRepo.Update(user);
            } else
            {
                throw new ElementDoesNotExistException("Unknown token", input.Token);
            }
        }
    }
}