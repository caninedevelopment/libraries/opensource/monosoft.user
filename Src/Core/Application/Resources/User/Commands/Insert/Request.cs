namespace Monosoft.User.Application.Resources.User.Commands.Insert
{
    using Monosoft.User.Application.Common.Helpers;
    using Monosoft.User.Application.Resources.User.Common;
    using Monosoft.Common.Command.Interfaces;
    using Monosoft.Common.Exceptions;
    using System;

    public class Request : UserDTO, IDtoRequest
    {
        public string Password { get; set; }


        public void Validate()
        {
            if (string.IsNullOrEmpty(Password))
            {
                throw new ValidationException("Password");
            }

            if (string.IsNullOrEmpty(Username))
            {
                throw new ValidationException("Username");
            }

            if (Validators.ValidEmail(Email) == false)
            {
                // TODO is this correct? At least it shouldnt throw exception
                Email = null;
            }

            if (Validators.ValidPhone(Mobile) == false)
            {
                // TODO is this correct? At least it shouldnt throw exception
                Mobile = null;
            }

            if (UserId == Guid.Empty)
            {
                throw new ValidationException("UserId");
            }
        }
    }
}