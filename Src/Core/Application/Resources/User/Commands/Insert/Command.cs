namespace Monosoft.User.Application.Resources.User.Commands.Insert
{
    using Monosoft.User.Application.Common.Helpers;
    using Domain.Interfaces;
    using Monosoft.Common.Command.Interfaces;
    using Monosoft.Common.Exceptions;
    using System.Linq;
    using Newtonsoft.Json;

    public class Command : IProcedure<Request>
    {
        private readonly IUserRepository uRepo;

        public Command(
            IUserRepository uRepo)
        {
            this.uRepo = uRepo;
        }

        public void Execute(Request input)
        {
            var user = uRepo.GetByUsername(input.Username);
            if (user == null)
            {
                user = uRepo.GetById(input.UserId);
                if (user == null)
                {
                    user = new Domain.Entities.User()
                    {
                        Id = input.UserId,
                        Username = input.Username,
                        Email = input.Email,
                        Mobile = input.Mobile,
                        MetadataString = JsonConvert.SerializeObject(input.Metadata),
                        PWDHash = Hash.CalculateHash(input.Password, input.UserId.ToString())
                    };

                    uRepo.Insert(user);
                } else
                {
                    throw new ElementAlreadyExistsException("UserId is already in use", input.UserId.ToString());
                }
            } else
            {
                throw new ElementAlreadyExistsException("Username already taken", input.Username);
            }
        }
    }
}