namespace Monosoft.User.Application.Resources.User.Commands.Get
{
    using Monosoft.User.Application.Resources.User.Common;
    using Domain.Entities;
    using Monosoft.Common.Command.Interfaces;
    using System.Collections.Generic;
    using System.Linq;
    using Newtonsoft.Json;

    public class Response : UserDTO, IDtoResponse
    {
        public List<string> Claims { get; set; }

        public Response(User user, IEnumerable<string> claims)
        {
            this.UserId = user.Id;
            this.Username = user.Username;
            this.Email = user.Email;
            this.Mobile = user.Mobile;
            this.Claims = claims.ToList();
            this.Metadata = string.IsNullOrEmpty(user.MetadataString) ? null : JsonConvert.DeserializeObject<List<MetadataDTO>>(user.MetadataString);
        }
    }
}