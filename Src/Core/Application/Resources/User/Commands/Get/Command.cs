namespace Monosoft.User.Application.Resources.User.Commands.Get
{
    using Domain.Interfaces;
    using Monosoft.Common.Command.Interfaces;
    using Monosoft.Common.Exceptions;
    using System.Linq;

    public class Command : IFunction<Request, Response>
    {
        private readonly IUserRepository uRepo;
        private readonly IUserInUserGroupRepository uinugRepo;
        private readonly IUserGroupRepository ugRepo;

        public Command(
            IUserRepository uRepo,
            IUserInUserGroupRepository uinugRepo,
            IUserGroupRepository ugRepo)
        {
            this.uRepo = uRepo;
            this.uinugRepo = uinugRepo;
            this.ugRepo = ugRepo;
        }

        public Response Execute(Request input)
        {
            var user = uRepo.GetById(input.UserId);
            if (user != null)
            {
                var ugids = uinugRepo.GetUserGroupIds(input.UserId);
                var claims = ugRepo.GetByIds(ugids).SelectMany(x => x.Claims);

                return new Response(user, claims);
            } else
            {
                throw new ElementDoesNotExistException("No user with that userid", input.UserId.ToString());
            }
        }
    }
}