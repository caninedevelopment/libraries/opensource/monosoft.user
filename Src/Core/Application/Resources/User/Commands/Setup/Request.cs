namespace Monosoft.User.Application.Resources.User.Commands.Setup
{
    using Monosoft.User.Application.Resources.User.Common;
    using Monosoft.Common.Command.Interfaces;
    public class Request : IDtoRequest
    {
        public int smtpPort { get; set; }

        public string smtpServerName { get; set; }

        public string smtpUserName { get; set; }

        public string smtpPassword { get; set; }

        public string anonymUserGroup { get; set; }

        public ForgotPasswordSettingsDTO ForgotPasswordSettings { get; set; }

        public void Validate()
        {
        }
    }
}