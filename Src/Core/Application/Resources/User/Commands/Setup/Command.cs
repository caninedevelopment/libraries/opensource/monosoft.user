namespace Monosoft.User.Application.Resources.User.Commands.Setup
{
    using Domain.Entities;
    using Domain.Interfaces;
    using Monosoft.Common.Command.Interfaces;

    public class Command : IProcedure<Request>
    {
        private readonly ISettingsRepository setRepo;

        public Command(
            ISettingsRepository setRepo)
        {
            this.setRepo = setRepo;
        }

        public void Execute(Request input)
        {
            var settings = setRepo.GetSettings();

            if (settings == null)
            {
                settings = new Settings();
            }

            settings.ForgotPasswordEmailBody = input.ForgotPasswordSettings.Body;
            settings.ForgotPasswordEmailSubject = input.ForgotPasswordSettings.Subject;
            settings.ForgotPasswordNameOfSender = input.ForgotPasswordSettings.NameOfSender;
            settings.ForgotPasswordSenderEmail = input.ForgotPasswordSettings.SenderEmail;
            settings.SMTPPassword = input.smtpPassword;
            settings.SMTPPort = input.smtpPort;
            settings.SMTPServerName = input.smtpServerName;
            settings.SMTPUserName = input.smtpUserName;
            settings.AnonymUserGroup = input.anonymUserGroup;

            setRepo.SetSettings(settings);
        }
    }
}