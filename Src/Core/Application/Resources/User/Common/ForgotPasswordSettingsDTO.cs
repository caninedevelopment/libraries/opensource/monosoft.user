﻿namespace Monosoft.User.Application.Resources.User.Common
{
    public class ForgotPasswordSettingsDTO
    {
        public string Subject { get; set; }
        public string Body { get; set; }
        public string SenderEmail { get; set; }
        public string NameOfSender { get; set; }
    }
}
