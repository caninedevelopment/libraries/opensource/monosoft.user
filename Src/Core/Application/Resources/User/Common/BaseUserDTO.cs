﻿namespace Monosoft.User.Application.Resources.User.Common
{
    using System;

    public class BaseUserDTO
    {
        public Guid UserId { get; set; }

        public string Username { get; set; }

        public string Email { get; set; }

        public string Mobile { get; set; }
    }
}
