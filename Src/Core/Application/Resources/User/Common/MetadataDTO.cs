﻿namespace Monosoft.User.Application.Resources.User.Common
{
    public class MetadataDTO
    {
        public string Key { get; set; }
        public string Value { get; set; }

        public MetadataDTO() { }

        public MetadataDTO(string key, string value)
        {
            this.Key = key;
            this.Value = value;
        }
    }
}
