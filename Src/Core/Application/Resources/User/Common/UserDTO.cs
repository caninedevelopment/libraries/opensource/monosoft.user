﻿namespace Monosoft.User.Application.Resources.User.Common
{
    using Newtonsoft.Json;
    using System.Collections.Generic;
    using System.Linq;

    public class UserDTO : BaseUserDTO
    {
        public List<MetadataDTO> Metadata { get; set; }

        public UserDTO() { }

        public UserDTO(Domain.Entities.User user)
        {
            this.Email = user.Email;
            this.Mobile = user.Mobile;
            this.Username = user.Username;
            this.UserId = user.Id;
            this.Metadata = string.IsNullOrEmpty(user.MetadataString) ? null : JsonConvert.DeserializeObject<List<MetadataDTO>>(user.MetadataString);
        }
    }
}
