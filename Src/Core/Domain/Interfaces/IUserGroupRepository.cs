﻿namespace Monosoft.User.Domain.Interfaces
{
    using Monosoft.User.Domain.Entities;
    using System;
    using System.Collections.Generic;

    public interface IUserGroupRepository
    {
        void Insert(UserGroup usergroup);
        void Update(UserGroup usergroup);
        UserGroup GetById(Guid id);
        List<UserGroup> GetByIds(List<Guid> ugIds);
        List<UserGroup> GetAll();
        void Delete(UserGroup usergroup);
    }
}
