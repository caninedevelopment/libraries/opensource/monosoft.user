﻿namespace Monosoft.User.Domain.Interfaces
{
    using Monosoft.User.Domain.Entities;
    using System;
    using System.Collections.Generic;

    public interface ITokenRepository
    {
        void Insert(Token token);
        void Update(Token token);
        Token GetById(Guid id);
        List<Token> GetByUserIdFromDateTime(Guid userId, DateTime fromDateTime);
    }
}
