﻿namespace Monosoft.User.Domain.Interfaces
{
    using Monosoft.User.Domain.Entities;
    using System;
    using System.Collections.Generic;

    public interface IUserRepository
    {
        void Insert(User user);
        void Update(User user);
        void Delete(User user);
        User GetByUsername(string username);
        User GetById(Guid id);
        User GetByEmail(string email);
        User GetByResetToken(string token);
        List<User> GetAll();
    }
}
