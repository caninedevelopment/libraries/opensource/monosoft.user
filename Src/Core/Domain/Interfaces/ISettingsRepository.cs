﻿namespace Monosoft.User.Domain.Interfaces
{
    using Monosoft.User.Domain.Entities;

    public interface ISettingsRepository
    {
        Settings GetSettings();
        void SetSettings(Settings set);
    }
}
