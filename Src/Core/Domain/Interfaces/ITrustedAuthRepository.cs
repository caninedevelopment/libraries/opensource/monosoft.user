﻿namespace Monosoft.User.Domain.Interfaces
{
    using Monosoft.User.Domain.Entities;

    public interface ITrustedAuthRepository
    {
        void Insert(TrustedAuth trustedauth);
        TrustedAuth GetByUrl(string url);
        void Delete(string url);
    }
}
