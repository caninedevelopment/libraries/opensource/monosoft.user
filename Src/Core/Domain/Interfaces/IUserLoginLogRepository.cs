﻿namespace Monosoft.User.Domain.Interfaces
{
    using Monosoft.User.Domain.Entities;

    public interface IUserLoginLogRepository
    {
        void Insert(UserLoginLog log);

    }
}
