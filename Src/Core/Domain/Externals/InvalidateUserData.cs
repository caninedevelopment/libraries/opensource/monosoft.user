﻿namespace Monosoft.User.Domain.Externals
{
    using System;

    public class InvalidateUserData
    {
        public Guid[] UserIds { get; set; }

        public DateTime ValidUntil { get; set; }
    }
}
