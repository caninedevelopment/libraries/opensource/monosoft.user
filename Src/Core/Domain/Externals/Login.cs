﻿namespace Monosoft.User.Domain.Externals
{
    public class LoginInfo
    {
        public string Username { get; set; }
        public string Password { get; set; }

        public LoginInfo(string username, string password)
        {
            this.Username = username;
            this.Password = password;
        }
    }
}
