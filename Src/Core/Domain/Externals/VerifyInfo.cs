﻿namespace Monosoft.User.Domain.Externals
{
    using System;

    public class VerifyInfo
    {
        public Guid TokenId { get; set; }

        public VerifyInfo(Guid tokenId)
        {
            this.TokenId = tokenId;
        }
    }
}
