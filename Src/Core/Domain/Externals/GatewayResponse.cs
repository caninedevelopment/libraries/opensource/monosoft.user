﻿namespace Monosoft.User.Domain.Externals
{
    using Monosoft.Common;

    public class GatewayResponse
    {
        public LocalizedString[] Message { get; set; }
        public bool Success { get; set; }
        public string Data { get; set; }
        public string DataType { get; set; }
        public string ResponseToClientId { get; set; }
        public string ResponseToMessageId { get; set; }
    }
}
