﻿namespace Monosoft.User.Domain.Externals
{
    using System;
    using System.Collections.Generic;

    public class TokenInfo
    {
        public Guid TokenId { get; set; }
        public Guid UserId { get; set; }
        public DateTime ValidUntil { get; set; }
        public List<string> Claims { get; set; }
    }
}
