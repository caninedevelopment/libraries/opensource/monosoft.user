﻿namespace Monosoft.User.Domain.Entities
{
    using System;
    using System.Collections.Generic;

    public class TrustedAuth
    {
        public Guid Id { get; set; }
        public string ServerUrl { get; set; }
        public List<string> Trust { get; set; }

        public TrustedAuth() { }

        public TrustedAuth(string serverUrl, List<string> trust)
        {
            this.Id = Guid.NewGuid();
            this.ServerUrl = serverUrl;
            this.Trust = trust;
        }
    }
}
