﻿namespace Monosoft.User.Domain.Entities
{
    using System;
    
    public class UserInUserGroup
    {
        public Guid Id { get; set; }

        public Guid FK_User { get; set; }

        public Guid FK_UserGroup { get; set; }

        public UserInUserGroup() { }

        public UserInUserGroup(Guid fk_user, Guid fk_usergroup)
        {
            this.Id = Guid.NewGuid();
            this.FK_User = fk_user;
            this.FK_UserGroup = fk_usergroup;
        }
    }
}
