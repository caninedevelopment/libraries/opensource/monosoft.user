﻿namespace Monosoft.User.Domain.Entities
{
    using System;
    using System.Collections.Generic;

    public class User
    {
        public Guid Id { get; set; }

        public string Username { get; set; }

        public string PWDHash { get; set; }

        public string Email { get; set; }

        public string Mobile { get; set; }

        public string ResetToken { get; set; }

        public List<string> Claims { get; set; }

        public string MetadataString { get; set; }
    }
}
