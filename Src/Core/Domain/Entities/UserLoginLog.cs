﻿namespace Monosoft.User.Domain.Entities
{
    using System;

    public class UserLoginLog
    {
        public Guid Id { get; set; }
        public DateTime Date { get; set; }
        public string Ip { get; set; }
        public bool Success { get; set; }
        public User User { get; set; }

        public UserLoginLog() { }

        public UserLoginLog(DateTime datetime, string ip, bool success, User user)
        {
            this.Id = Guid.NewGuid();
            this.Date = datetime;
            this.Ip = ip;
            this.Success = success;
            this.User = user;
        }
    }
}
