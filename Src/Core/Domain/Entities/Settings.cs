﻿using System;

namespace Monosoft.User.Domain.Entities
{
    public class Settings
    {
        public Guid Id { get; set; }
        public int SMTPPort { get; set; }
        public string SMTPServerName { get; set; }
        public string SMTPUserName { get; set; }
        public string SMTPPassword { get; set; }

        public string ForgotPasswordEmailSubject { get; set; }
        public string ForgotPasswordEmailBody { get; set; }
        public string ForgotPasswordSenderEmail { get; set; }
        public string ForgotPasswordNameOfSender { get; set; }

        public string AnonymUserGroup { get; set; }
    }
}
