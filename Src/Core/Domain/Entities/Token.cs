﻿namespace Monosoft.User.Domain.Entities
{
    using System;
    using System.Collections.Generic;

    public class Token
    {
        public Guid Id { get; set; }
        public Guid FK_User { get; set; }
        public DateTime ValidUntil { get; set; }
        public List<string> Claims { get; set; }

        public Token() { }

        public Token(Guid id, Guid user, DateTime validUntil, List<string> claims)
        {
            this.Id = id;
            this.FK_User = user;
            this.ValidUntil = validUntil;
            this.Claims = claims;
        }
    }
}
