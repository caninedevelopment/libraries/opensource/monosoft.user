﻿namespace Monosoft.User.Domain.Entities
{
    using System;
    using System.Collections.Generic;

    public class UserGroup
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public List<string> Claims { get; set; }
    }
}
