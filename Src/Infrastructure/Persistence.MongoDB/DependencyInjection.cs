namespace Monosoft.User.Persistence.MongoDB
{
    using Monosoft.User.Domain.Interfaces;
    using Microsoft.Extensions.DependencyInjection;
    using Persistence.MongoDB.Repositories;

    public static class DependencyInjection
    {
        public static IServiceCollection AddMongoDBPersistenceProvider(this IServiceCollection services, string dbServerHost, string dbUser, string dbPassword, int dbServerPort, string dbName)
        {
            var context = new DbContext(dbUser, dbPassword, dbServerHost, dbServerPort, dbName);
            services.AddSingleton<DbContext>(context);

            services.AddSingleton<IUserRepository, UserRepository>();
            services.AddSingleton<IUserGroupRepository, UserGroupRepository>();
            services.AddSingleton<IUserInUserGroupRepository, UserInUserGroupRepository>();
            services.AddSingleton<ITokenRepository, TokenRepository>();
            services.AddSingleton<ISettingsRepository, SettingsRepository>();
            services.AddSingleton<IUserLoginLogRepository, UserLoginLogRepository>();
            services.AddSingleton<ITrustedAuthRepository, TrustedAuthRepository>();

            return services;
        }
    }
}