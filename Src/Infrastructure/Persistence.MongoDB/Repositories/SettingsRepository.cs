﻿namespace Monosoft.User.Persistence.MongoDB.Repositories
{
    using Monosoft.User.Domain.Entities;
    using Monosoft.User.Domain.Interfaces;
    using global::MongoDB.Driver;

    public class SettingsRepository : ISettingsRepository
    {
        private readonly DbContext context;

        public SettingsRepository(DbContext context)
        {
            this.context = context;
        }

        const string collectionName = "settings";

        private IMongoCollection<Settings> collection
        {
            get { return this.context.db.GetCollection<Settings>(collectionName); }
        }

        public Settings GetSettings()
        {
            return collection.AsQueryable().FirstOrDefault();
        }

        public void SetSettings(Settings set)
        {
            var empty = new System.Guid("00000000-0000-0000-0000-000000000000");
            set.Id = empty;
            var col = collection;
            var setting = col.Find(x => x.Id == empty).FirstOrDefault();
            if (setting != null)
            {
                var filter = Builders<Settings>.Filter.Eq("Id", 1);
                col.ReplaceOne(
                    filter,
                    set
                );
            } else
            {
                col.InsertOne(set);
            }
        }
    }
}
