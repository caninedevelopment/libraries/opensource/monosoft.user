﻿namespace Monosoft.User.Persistence.MongoDB.Repositories
{
    using Monosoft.User.Domain.Entities;
    using Monosoft.User.Domain.Interfaces;
    using global::MongoDB.Driver;
    using System;
    using System.Collections.Generic;

    public class UserGroupRepository : IUserGroupRepository
    {
        private readonly DbContext context;

        public UserGroupRepository(DbContext context)
        {
            this.context = context;
        }

        const string collectionName = "usergroup";

        private IMongoCollection<UserGroup> collection
        {
            get { return this.context.db.GetCollection<UserGroup>(collectionName); }
        }

        public void Insert(UserGroup usergroup)
        {
            collection.InsertOne(usergroup);
        }

        public void Update(UserGroup usergroup)
        {
            collection.ReplaceOne(x => x.Id == usergroup.Id, usergroup);
        }

        public UserGroup GetById(Guid id)
        {
            return collection.Find(x => x.Id == id).FirstOrDefault();
        }

        public List<UserGroup> GetByIds(List<Guid> ugIds)
        {
            return collection.Find(x => ugIds.Contains(x.Id)).ToList();
        }

        public List<UserGroup> GetAll()
        {
            return collection.AsQueryable().ToList();
        }

        public void Delete(UserGroup usergroup)
        {
            collection.DeleteOne(x => x.Id == usergroup.Id);
        }
    }
}
