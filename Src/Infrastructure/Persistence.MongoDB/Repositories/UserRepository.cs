﻿namespace Monosoft.User.Persistence.MongoDB.Repositories
{
    using Monosoft.User.Domain.Entities;
    using Monosoft.User.Domain.Interfaces;
    using global::MongoDB.Driver;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class UserRepository : IUserRepository
    {
        private readonly DbContext context;

        public UserRepository(DbContext context)
        {
            this.context = context;
        }

        const string collectionName = "user";

        private IMongoCollection<User> collection
        {
            get { return this.context.db.GetCollection<User>(collectionName); }
        }

        public void Delete(User user)
        {
            collection.DeleteOne(x => x.Id == user.Id);
        }

        public List<User> GetAll()
        {
            return collection.AsQueryable().ToList();
        }

        public User GetByEmail(string email)
        {
            return collection.Find(x => x.Email == email).FirstOrDefault();
        }

        public User GetById(Guid id)
        {
            return collection.Find(x => x.Id == id).FirstOrDefault();
        }

        public User GetByResetToken(string token)
        {
            return collection.Find(x => x.ResetToken == token).FirstOrDefault();
        }

        public User GetByUsername(string username)
        {
            return collection.Find(x => x.Username == username).FirstOrDefault();
        }

        public void Insert(User user)
        {
            collection.InsertOne(user);
        }

        public void Update(User user)
        {
            collection.ReplaceOne(x => x.Id == user.Id, user);
        }
    }
}
