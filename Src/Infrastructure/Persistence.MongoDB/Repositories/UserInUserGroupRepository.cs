﻿namespace Monosoft.User.Persistence.MongoDB.Repositories
{
    using Monosoft.User.Domain.Entities;
    using Monosoft.User.Domain.Interfaces;
    using global::MongoDB.Driver;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class UserInUserGroupRepository : IUserInUserGroupRepository
    {
        private readonly DbContext context;

        public UserInUserGroupRepository(DbContext context)
        {
            this.context = context;
        }

        const string collectionName = "userinusergroup";

        private IMongoCollection<UserInUserGroup> collection
        {
            get { return this.context.db.GetCollection<UserInUserGroup>(collectionName); }
        }

        public void Delete(UserInUserGroup uinug)
        {
            collection.DeleteOne(x => x.Id == uinug.Id);
        }

        public void DeleteByUserGroup(Guid ugId)
        {
            collection.DeleteMany(x => x.FK_UserGroup == ugId);
        }

        public UserInUserGroup GetByUserAndUserGroup(Guid userId, Guid usergroupId)
        {
            return collection.Find(x => x.FK_User == userId && x.FK_UserGroup == usergroupId).FirstOrDefault();
        }

        public List<UserInUserGroup> GetByUserGroupId(Guid ugId)
        {
            return collection.Find(x => x.FK_UserGroup == ugId).ToList();
        }

        public List<Guid> GetUserGroupIds(Guid userId)
        {
            // TODO better way to select? (Instead of ToList before
            return collection.Find(x => x.FK_User == userId).ToList().Select(x => x.FK_UserGroup).ToList();
        }

        public void Insert(UserInUserGroup uinug)
        {
            collection.InsertOne(uinug);
        }
    }
}
