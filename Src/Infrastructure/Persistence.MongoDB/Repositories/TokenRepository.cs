﻿namespace Monosoft.User.Persistence.MongoDB.Repositories
{
    using Monosoft.User.Domain.Entities;
    using Monosoft.User.Domain.Interfaces;
    using global::MongoDB.Driver;
    using System;
    using System.Collections.Generic;

    public class TokenRepository : ITokenRepository
    {
        private readonly DbContext context;

        public TokenRepository(DbContext context)
        {
            this.context = context;
        }

        const string collectionName = "token";

        private IMongoCollection<Token> collection
        {
            get { return this.context.db.GetCollection<Token>(collectionName); }
        }

        public Token GetById(Guid id)
        {
            return collection.Find(x => x.Id == id).FirstOrDefault();
        }

        public List<Token> GetByUserIdFromDateTime(Guid userId, DateTime fromDateTime)
        {
            return collection.Find(x => x.FK_User == userId && x.ValidUntil > fromDateTime).ToList();
        }

        public void Insert(Token token)
        {
            collection.InsertOne(token);
        }

        public void Update(Token token)
        {
            var filter = Builders<Token>.Filter.Eq("Id", token.Id);
            collection.ReplaceOne(
                filter,
                token
            );
        }
    }
}
