﻿namespace Monosoft.User.Persistence.MongoDB.Repositories
{
    using Monosoft.User.Domain.Entities;
    using Monosoft.User.Domain.Interfaces;
    using global::MongoDB.Driver;

    public class TrustedAuthRepository : ITrustedAuthRepository
    {
        private readonly DbContext context;

        public TrustedAuthRepository(DbContext context)
        {
            this.context = context;
        }

        const string collectionName = "trustedauth";

        private IMongoCollection<TrustedAuth> collection
        {
            get { return this.context.db.GetCollection<TrustedAuth>(collectionName); }
        }

        public void Delete(string url)
        {
            collection.DeleteOne(x => x.ServerUrl == url);
        }

        public TrustedAuth GetByUrl(string url)
        {
            return collection.Find(x => x.ServerUrl == url).FirstOrDefault();
        }

        public void Insert(TrustedAuth trustedauth)
        {
            collection.InsertOne(trustedauth);
        }
    }
}
