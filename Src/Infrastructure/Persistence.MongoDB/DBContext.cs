﻿namespace Monosoft.User.Persistence.MongoDB
{
    using global::MongoDB.Bson.Serialization.Conventions;
    using global::MongoDB.Driver;

    public class DbContext
    {
        internal IMongoDatabase db;

        public DbContext(string username, string password, string host, int port, string dbName)
        {
            var customConventions = new ConventionPack {
                new IgnoreExtraElementsConvention(true), // Ignore the extra fields that a document has when compared to the provided DTO on Deserialization.  
                new IgnoreIfDefaultConvention(true) // Ignore the fields with default value on Serialization. Keeps the document smaller.
            };
            ConventionRegistry.Register("CustomConventions", customConventions, type => true);

            var client = new MongoClient($"mongodb://{username}:{password}@{host}:{port}");
            db = client.GetDatabase(dbName); // automatically creates a database if it doesn't exists
        }
    }
}
