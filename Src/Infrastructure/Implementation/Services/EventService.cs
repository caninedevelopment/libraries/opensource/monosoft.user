﻿namespace Monosoft.User.Implementation.Services
{
    using Canine.Net.Infrastructure.RabbitMQ.Event;
    using Monosoft.User.Application.Common.Helpers;
    using Monosoft.User.Application.Common.Services;

    public class EventService : IEventService
    {
        public void RaiseEvent(string route, string json)
        {
            EventClient.Instance.RaiseEvent(GlobalValues.RouteTokenInvalidateToken, new Canine.Net.Infrastructure.RabbitMQ.Message.ReturnMessage(json));
        }
    }
}
