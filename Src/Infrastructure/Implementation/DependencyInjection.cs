namespace Monosoft.User.Implementation
{
    using Microsoft.Extensions.DependencyInjection;
    using Monosoft.User.Application.Common.Services;
    using Monosoft.User.Implementation.Services;

    public static class DependencyInjection
    {
        public static IServiceCollection AddImplementation(this IServiceCollection services)
        {
            services.AddSingleton<IEventService, EventService>();

            return services;
        }
    }
}