﻿namespace Monosoft.User.Persistence.PostgreSQL
{
    using Monosoft.User.Domain.Entities;
    using Microsoft.EntityFrameworkCore;
    using System.Linq;
    using System;
    using Monosoft.User.Application.Common.Helpers;

    public class DbContext : Microsoft.EntityFrameworkCore.DbContext
    {
        public DbContext(DbContextOptions<DbContext> options)
        : base(options) {
            this.Database.EnsureCreated();
            this.CreateDefaultEntities();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(DbContext).Assembly);
        }

        public DbSet<UserGroup> Usergroups { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Settings> Settings { get; set; }
        public DbSet<TrustedAuth> TrustedAuths { get; set; }
        public DbSet<Token> Tokens { get; set; }
        public DbSet<UserInUserGroup> UserInUserGroups { get; set; }
        public DbSet<UserLoginLog> UserLoginLogs { get; set; }

        void CreateDefaultEntities()
        {
            var pwd = "defaultPWD";
            var user = "root";
            var email = "info@monosoft.dk";

            var dbuser = Users.Where(x => x.Username == user).FirstOrDefault();
            if (dbuser == null)
            {
                var userId = Guid.NewGuid();
                var ugId = Guid.NewGuid();

                Users.Add(new User()
                {
                    Id = userId,
                    Username = user,
                    Email = email,
                    Mobile = "",
                    PWDHash = Hash.CalculateHash(pwd, userId.ToString())
                });

                Usergroups.Add(new UserGroup()
                {
                    Id = ugId,
                    Name = "Admin",
                    Claims = new System.Collections.Generic.List<string>()
                    {
                        "User.IsAdmin",
                        "User.IsOperator",
                        "User.IsConsumer",
                        "UserGroup.IsAdmin",
                        "UserGroup.IsOperator",
                        "UserGroup.IsConsumer"
                    }
                });

                UserInUserGroups.Add(new UserInUserGroup()
                {
                    Id = Guid.NewGuid(),
                    FK_User = userId,
                    FK_UserGroup = ugId
                });

                SaveChanges();
            }
        }
    }
}
