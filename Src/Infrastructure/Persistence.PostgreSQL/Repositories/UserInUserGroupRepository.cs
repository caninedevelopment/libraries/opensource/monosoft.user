﻿namespace Monosoft.User.Persistence.PostgreSQL.Repositories
{
    using Monosoft.User.Domain.Entities;
    using Monosoft.User.Domain.Interfaces;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class UserInUserGroupRepository : IUserInUserGroupRepository
    {
        private readonly DbContext db;

        public UserInUserGroupRepository(DbContext db)
        {
            this.db = db;
        }

        public void Delete(UserInUserGroup uinug)
        {
            db.UserInUserGroups.Remove(uinug);
            db.SaveChanges();
        }

        public void DeleteByUserGroup(Guid ugId)
        {
            var entities = db.UserInUserGroups.Where(x => x.FK_UserGroup == ugId);
            db.UserInUserGroups.RemoveRange(entities);
            db.SaveChanges();
        }

        public UserInUserGroup GetByUserAndUserGroup(Guid userId, Guid usergroupId)
        {
            return db.UserInUserGroups.Where(x => x.FK_User == userId && x.FK_UserGroup == usergroupId).FirstOrDefault();
        }

        public List<UserInUserGroup> GetByUserGroupId(Guid ugId)
        {
            return db.UserInUserGroups.Where(x => x.FK_UserGroup == ugId).ToList();
        }

        public List<Guid> GetUserGroupIds(Guid userId)
        {
            return db.UserInUserGroups.Where(x => x.FK_User == userId).Select(x => x.FK_UserGroup).ToList();
        }

        public void Insert(UserInUserGroup uinug)
        {
            db.UserInUserGroups.Add(uinug);
            db.SaveChanges();
        }
    }
}
