﻿namespace Monosoft.User.Persistence.PostgreSQL.Repositories
{
    using Monosoft.User.Domain.Entities;
    using Monosoft.User.Domain.Interfaces;
    using System.Linq;

    public class TrustedAuthRepository : ITrustedAuthRepository
    {
        private readonly DbContext db;

        public TrustedAuthRepository(DbContext db)
        {
            this.db = db;
        }

        public void Delete(string url)
        {
            var entity = db.TrustedAuths.Where(x => x.ServerUrl == url).FirstOrDefault();
            if (entity != null)
            {
                db.TrustedAuths.Remove(entity);
                db.SaveChanges();
            }
        }

        public TrustedAuth GetByUrl(string url)
        {
            return db.TrustedAuths.Where(x => x.ServerUrl == url).FirstOrDefault();
        }

        public void Insert(TrustedAuth trustedauth)
        {
            db.TrustedAuths.Add(trustedauth);
            db.SaveChanges();
        }
    }
}
