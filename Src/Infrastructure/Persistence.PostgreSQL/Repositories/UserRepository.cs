﻿namespace Monosoft.User.Persistence.PostgreSQL.Repositories
{
    using Monosoft.User.Domain.Interfaces;
    using Monosoft.User.Domain.Entities;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class UserRepository : IUserRepository
    {
        private readonly DbContext db;

        public UserRepository(DbContext db)
        {
            this.db = db;
        }

        public void Delete(User user)
        {
            db.Users.Remove(user);
            db.SaveChanges();
        }

        public List<User> GetAll()
        {
            return db.Users.ToList();
        }

        public User GetByEmail(string email)
        {
            return db.Users.Where(x => x.Email == email).FirstOrDefault();
        }

        public User GetById(Guid id)
        {
            return db.Users.Where(x => x.Id == id).FirstOrDefault();
        }

        public User GetByResetToken(string token)
        {
            return db.Users.Where(x => x.ResetToken == token).FirstOrDefault();
        }

        public User GetByUsername(string username)
        {
            return db.Users.Where(x => x.Username == username).FirstOrDefault();
        }

        public void Insert(User user)
        {
            db.Users.Add(user);
            db.SaveChanges();
        }

        public void Update(User user)
        {
            db.Entry(user).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            db.SaveChanges();
        }
    }
}