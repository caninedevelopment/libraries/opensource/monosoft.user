﻿namespace Monosoft.User.Persistence.PostgreSQL.Repositories
{
    using Monosoft.User.Domain.Entities;
    using Monosoft.User.Domain.Interfaces;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class TokenRepository : ITokenRepository
    {
        private readonly DbContext db;

        public TokenRepository(DbContext db)
        {
            this.db = db;
        }

        public Token GetById(Guid id)
        {
            return db.Tokens.Where(x => x.Id == id).FirstOrDefault();
        }

        public List<Token> GetByUserIdFromDateTime(Guid userId, DateTime fromDateTime)
        {
            return db.Tokens.Where(x => x.FK_User == userId && x.ValidUntil > fromDateTime).ToList();
        }

        public void Insert(Token token)
        {
            db.Tokens.Add(token);
            db.SaveChanges();
        }

        public void Update(Token token)
        {
            db.Entry(token).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            db.SaveChanges();
        }
    }
}
