﻿namespace Monosoft.User.Persistence.PostgreSQL.Repositories
{
    using Monosoft.User.Domain.Entities;
    using Monosoft.User.Domain.Interfaces;

    public class UserLoginLogRepository : IUserLoginLogRepository
    {
        private readonly DbContext db;

        public UserLoginLogRepository(DbContext db)
        {
            this.db = db;
        }

        public void Insert(UserLoginLog log)
        {
            db.UserLoginLogs.Add(log);
            db.SaveChanges();
        }
    }
}
