# Introduction
PLEASE WRITE AN INTRODUCTION TO YOUR LIBRARY HERE.
## Purpose
PLEASE WRITE THE PURPOSE OF YOUR LIBRARY
# Reference links
- [The Canine.Net project](https://canine.monosoft.dk/)
- [Monosoft development](https://dev.monosoft.dk/)
- [Gitlab yaml](https://docs.gitlab.com/ee/ci/yaml/).
If you're new to Canine.Net development you'll want to check out the documentation and the tutorial, but if you're
already a seasoned developer with solid .NET, "clean code" and "clean architecture" experience considering building your own .NET
application for Canine, this should all look very familiar.
## What's contained in this project
The repository folder structure (example/explanation):
```
    - docs                                      //the techical documentation is placed here, if there are any
    - src                                       //the source code is placed within this folder
        - core
            - application
                - [nameOfResource]              //
                    - commands
                        - get
                            command.cs
                            response.cs
                        - getmany
                            command.cs
                            request.cs
                            response.cs
                        - insert
                            command.cs
                            request.cs
            - domain
        - infrastructure
            - external                          //if the service is dependent on external ressources, these must be placed within this subfolder
                - [nameOfExternalService1]
                - [nameOfExternalService2]
            - Persistence.[nameOfTechnology1]
            - Persistence.[nameOfTechnology2]
    - test                                      //Unittest and integrations test are placed here, seperated from the business logic in src
    README.md                                   //this document
    .gitlab-ci.yml                              //the gitlab yaml file for CI/CD pipeline
    .gitignore                                  //the git ignore file for this repository
    LICENSE                                     //the license for this repository
```
